<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form class="cmxform" id="register" method="POST" action="data_faker.php">

Register

<p>
<label for="cemail">E-Mail</label>
<input id="cemail" type="email" name="cemail" required>
</p>
<p>
<label for="confirm_email">Confirm E-Mail</label>
<input id="confirm_email" type="email" name="confirm_email" required>
</p>
<p>
<label for="Fname">First name</label>
<input id="Fname" name="Fname"  type="text">
</p>
<p>
<label for="Lname">Last name</label>
<input id="Lname" name="Lname"  type="text">
</p>
<p>
<label for="password">Password</label>
<input id="password" type="text" name="password" required>
</p>
<p>
<label for="confirm_password">Confirm Password</label>
<input id="confirm_password" type="text" name="confirm_password" required>
</p>
<p>
<input class="submit" type="submit" value="Submit" name="bnt_submit">
</p>

</form>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.js"></script>

<script>

// check ตัวแรกเป็นพิมพ์ใหญ่
$.validator.addMethod( "FirstCharacter", function( value, element ) {
    if (this.optional(element)) {
        return true;
    } 
        return /[A-Z]/.test( value );
}, "First character must be capital letter" );

// check รหัสผ่าน เป็น char กับ Number
$.validator.addMethod("passwordchecked", function(value,element){
    if (this.optional(element)) {
        return true;
    }
        return /[A-Za-z0-9]/.test(value);
}, "The value must be character and number only");




$("#register").validate({
    rules: {
        cemail:{            
            required:true,
            email:true,
                      
        },
        confirm_email: {
            required:true,
            email:false,
            equalTo:'#cemail'
        },
        Fname:{            
            FirstCharacter:true
            
        },
        Lname:{            
            FirstCharacter:true
            
        },
        password:{            
            required:true,
            passwordchecked:true,
            minlength:6
            
            
        },
        confirm_password:{            
            required:true,
            equalTo:'#password'
        }
    },
    messages: {
        cemail:{            
            required:'กรุณาใส่อีเมล',
            email:'กรุณาใส่อีเมลให้ถูกต้อง'
                      
        },
        confirm_email: {
            required:'กรุณาใส่ยืนยันอีเมล',
            equalTo:'ยืนยันอีเมลไม่ถูกต้อง'
        },
        Fname:{            
            FirstCharacter:'กรุณากรอกข้อมูลเป็นภาษาอังกฤษ โดยตัวแรกเป็นพิมพ์ใหญ่'
        },
        Lname:{            
            FirstCharacter:'กรุณากรอกข้อมูลเป็นภาษาอังกฤษ โดยตัวแรกเป็นพิมพ์ใหญ่'
            
        },
        password:{            
            required:'กรุณาใส่ Password',
            passwordchecked:'กรุณากรอกเฉพาะตัวอักษรและตัวเลขเท่านั้น',
            minlength:'กรุณากรอกรหัสผ่าน อย่างน้อย 6 ตัว'
            
        },
        

        confirm_password:{            
            required:'กรุณายืนยันรหัสผ่าน',
            equalTo:'ยืนยันรหัสผ่านไม่ถูกต้อง'
        }
    },
    submitHandler: function(form) {
		// do other things for a valid form
		//alert('after check valid');
        form.submit();
    }


});


</script>
</body>
</html>